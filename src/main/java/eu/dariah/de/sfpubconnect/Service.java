package eu.dariah.de.sfpubconnect;

/*-
 * #%L
 * DARIAH Seafile DH-Publish Connector
 * %%
 * Copyright (C) 2016 SUB Göttingen (https://www.sub.uni-goettingen.de)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

public class Service {
	
	private Client seafileClient;
	private String seafileSecret;
	private String seafileLocation = "https://sftest.de.dariah.eu";
	
	public Service() {
		this.seafileClient = ClientBuilder
				.newBuilder().build()
				.register(JacksonJsonProvider.class)
				.property("thread.safe.client", "true");
	}
	
	@GET
	@Path("/{sfid}")
	public Response getFile(
			@PathParam("sfid") String sfid,
			@HeaderParam("Authorization") String authHeader,
			@QueryParam("token") String token
			) throws URISyntaxException, TikaException, IOException {
		
		if(authHeader != null) {
			token = authHeader.split(" ")[1];
		}
		
		String path = SeafileUtils.resolveIdString(sfid).path;
		String sflibid = SeafileUtils.resolveIdString(sfid).libraryId;
		String sfToken = tokenFromAuthstring(token, this.seafileSecret);
		
		InputStream is = this.readSFileToInputStream(sflibid, sfid, sfToken);
		
		TikaConfig tika = new TikaConfig();

		Metadata metadata = new Metadata();
		metadata.set(Metadata.RESOURCE_NAME_KEY, path);
		MediaType mediatype = tika.getDetector().detect(is, metadata);
		
		System.out.println(mediatype.toString());
		
		return Response.ok(is).header("Content-Type", mediatype.toString()).build();
	}
	
	public InputStream readSFileToInputStream(String sflibid, String sfFileId, String token) throws URISyntaxException {
	
		String filepath = new String(SeafileUtils.resolveIdString(sfFileId).path);	
		String link = getDownloadUrl(sflibid, filepath, token);
		link = link.replaceAll("\"", "");
		return download(new URI(link));

	}
	
	public InputStream download(URI uri) {
		return seafileClient.target(this.seafileLocation)
				.path(uri.getPath()).request().get().readEntity(InputStream.class);
	}
	
	public String getDownloadUrl(String libId, String path, String token) {
		System.out.println("getFile: " + path + " from lib: " + libId);
		System.out.println(" token: " + token);
		return seafileClient.target(this.seafileLocation)
				.path("/api2/repos/"+libId+"/file/").queryParam("p", path).request()
				.header("Authorization", "Token " + token).get().readEntity(String.class);
	}
	
	public static String tokenFromAuthstring(String authString, String secret) {
		Jws<Claims> jwtres = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(authString);
		String tstring = jwtres.getBody().get("token").toString();
		String token = tstring.substring(tstring.lastIndexOf("@")+1);
		return token;
	}
	
	public String getSeafileSecret() {
		return seafileSecret;
	}

	public void setSeafileSecret(String seafileSecret) {
		System.out.println("secret set");
		this.seafileSecret = seafileSecret;
	}

	public String getSeafileLocation() {
		return seafileLocation;
	}

	public void setSeafileLocation(String seafileLocation) {
		System.out.println("location set to: " + seafileLocation);
		this.seafileLocation = seafileLocation;
	}

}
